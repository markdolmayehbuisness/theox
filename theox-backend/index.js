import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import connectDB from "./src/DB/db.js";

dotenv.config();

import UserRoutes from "./src/Routes/userRoutes.js";

import { notFound, errorHandler } from "./src/Middlewares/errorMiddleware.js";

const app = express();
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use(express.static("uploads"));

connectDB();

app.use("/api/users", UserRoutes);

app.get("/", (req, res) => {
  if (res) {
    res.send("API is healthy");
  } else {
    res.send("API is down");
  }
});

app.use(notFound);
app.use(errorHandler);
app.listen(port, () => console.log(`server running on port ${port}`));
