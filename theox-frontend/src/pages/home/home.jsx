import React from "react";
import "./home.css";
import HomeHero from "../../components/homeHero/HomeHero";
import HomeMain from "../../components/homeMain/homeMain";
function Home() {
  return (
    <div className="home-container">
      <HomeHero></HomeHero>
      <HomeMain></HomeMain>
    </div>
  );
}

export default Home;
