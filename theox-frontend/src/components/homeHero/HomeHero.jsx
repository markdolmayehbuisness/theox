import React, { useEffect } from "react";
import "./homeHero.css";
import oxhead from "../../assets/bull4.png";
import { Link } from "react-router-dom";

import AOS from "aos";
import "aos/dist/aos.css";

AOS.init();
function HomeHero() {
  return (
    <div className="hh-container">
      <img
        src={oxhead}
        height={"150px"}
        alt="aux logo"
        className="hh-logo"
        data-aos="fade-down"
        data-aos-duration="700"
      />
      <p data-aos="fade-down" data-aos-duration="700" data-aos-delay="100">
        The modern method to learn guitar and music theory.
      </p>
      <div className="button-container">
        <Link
          data-aos="fade-down"
          data-aos-duration="700"
          data-aos-delay="200"
          to="/learn"
        >
          Learn
        </Link>
        <Link
          data-aos="fade-down"
          data-aos-duration="700"
          data-aos-delay="200"
          to="mailto:markdolmayehdev@gmail.com"
          href="mailto:max.mustermann@example.com?body=My custom mail body"
        >
          Contact
        </Link>
      </div>
    </div>
  );
}

export default HomeHero;
