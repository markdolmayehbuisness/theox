import React from "react";
import "./homeMain.css";
import { Link } from "react-router-dom";

import AOS from "aos";
import "aos/dist/aos.css";

AOS.init();

function HomeMain() {
  return (
    <div className="hm-container">
      <h2 className="hm-title">Our Method:</h2>
      <h3 className="hm-text">
        Here at The Aux we aim to take you through learning the guitar via a
        step by step instruction system where you are offered a follow with type
        manual to learn to play your favourite songs and practice daily drills.
        Earn points while practicing you favourite instrument and climb the
        leaderboard to get to the top. Top players are given prizes and rewards
        as well as their name in our hall of fame!
      </h3>
      <div className="genre-container">
        <div className="hm-genre">
          <Link
            to={"/learn"}
            onClick={() => {
              scrollTo(0, 0);
            }}
          >
            <h1>Flamenco</h1>
          </Link>
        </div>
        <div className="hm-genre">
          <Link
            to={"/learn"}
            onClick={() => {
              scrollTo(0, 0);
            }}
          >
            <h1>Rock</h1>
          </Link>
        </div>
        <div className="hm-genre">
          <Link
            to={"/learn"}
            onClick={() => {
              scrollTo(0, 0);
            }}
          >
            <h1>Jazz</h1>
          </Link>
        </div>
        <div className="hm-genre">
          <Link
            to={"/learn"}
            onClick={() => {
              scrollTo(0, 0);
            }}
          >
            <h1>Pop</h1>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default HomeMain;
