import React, { useState } from "react";
import "./nav.css";
import { Link } from "react-router-dom";
import AOS from "aos";
import "aos/dist/aos.css";
AOS.init();

function Nav() {
  const [burgerToggled, setBurgerToggled] = useState(false);
  return (
    <div className="nav-container">
      <div className="nav-logo">
        <Link
          to="/"
          data-aos="fade-right"
          data-aos-duration="400"
          onClick={() => {
            scrollTo(0, 0);
          }}
        >
          The AUX
        </Link>
      </div>
      {/* <a
        id="nav-toggle"
        href="#"
        onClick={() => {
          burgerToggled ? setBurgerToggled(false) : setBurgerToggled(true);
        }}
      >
        <span></span>
      </a>
      <div className={burgerToggled ? "nav-links-toggled" : "nav-links"}>
        <div className="nav-link-container">
          <Link
            data-aos="fade-left"
            data-aos-duration="600"
            data-aos-delay="100"
            to="learn"
          >
            <p>Learn</p>
          </Link>
          <Link
            data-aos="fade-left"
            data-aos-duration="600"
            data-aos-delay="200"
            to="about"
          >
            <p>About</p>
          </Link>
          <Link
            data-aos="fade-left"
            data-aos-duration="600"
            data-aos-delay="300"
            to="contact"
          >
            <p>Contact us</p>
          </Link>
        </div>
      </div> */}
    </div>
  );
}

export default Nav;
